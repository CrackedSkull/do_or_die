from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from .managers import CustomUserManager
from django.core.validators import EmailValidator
from django.utils.timezone import now
from tinymce.models import HTMLField


class CustomUser(AbstractUser):
    email_validator = EmailValidator()
    username = None
    email = models.EmailField(_('email'),
                              validators=[email_validator],
                              unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Tasks(models.Model):
    CHOISE_STATUS = (
        (None, 'Choose status of task'),
        (True, 'Done'),
        (False, "Don't yet")
    )

    person = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='person_task')
    title = models.CharField(max_length=250, verbose_name='Title', blank=False)
    task = HTMLField(max_length=1500, verbose_name='Task description', blank=False)
    created_date = models.DateField(auto_now_add=True, verbose_name='Created date')
    deadline = models.DateField(verbose_name='Deadline', default=now, blank=False)
    status = models.BooleanField(choices=CHOISE_STATUS, default=False, verbose_name='Result')

    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'
        ordering = ['deadline']

    def __str__(self):
        return self.person.email + ' ' + self.title + ' ' + str(self.created_date)
