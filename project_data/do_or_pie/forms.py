from django.contrib.auth.forms import UserCreationForm, UserChangeForm, SetPasswordForm
from .models import CustomUser, Tasks
from django import forms
from django.contrib.auth import authenticate, get_user_model, password_validation
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from tinymce.widgets import TinyMCE


class CustomUserCreationForm(UserCreationForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'}),
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}),
        strip=False,
    )
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'placeholder': 'Email'})
    )
    
    class Meta:
        model = CustomUser
        fields = ('email',)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            self.add_error(None, {"password2": self.error_messages['password_mismatch'], "password1": ''})
        try:
            password_validation.validate_password(password2, self.instance)
        except ValidationError as error:
            self.add_error(None, {"password2": error, "password1": ''})
        return password2
    
    def _post_clean(self):
        super(UserCreationForm, self)._post_clean()
 
    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if email == 'crackedskull@protonmail.com':
            self.add_error(None, {'email': 'A user with that username already exists.'})
        return email


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('email',)


class CustomAuthForm(forms.Form):
    username = forms.EmailField(
        widget=forms.EmailInput(attrs={'autofocus': 'True', 'placeholder': 'Email'}),
    )
    password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'}),
    )
    error_messages = {
        'invalid_login': _(
            "Please enter a correct username and password."
        ),
        'inactive': _("This account is inactive."),
    }
    
    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)
    
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        
        if username is not None and password:
            try:
                self.user_cache = get_user_model()._default_manager.get_by_natural_key(username)
            except ObjectDoesNotExist:
                raise self.get_invalid_login_error()
            self.confirm_login_allowed(self.user_cache)
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
        return self.cleaned_data
    
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                {'username': self.error_messages['inactive'], 'password': ''},
                code='inactive',
            )
    
    def get_invalid_login_error(self):
        return forms.ValidationError(
            {'username': self.error_messages['invalid_login'], 'password': ''},
            code='invalid_login',
        )
    
    def get_user(self):
        return self.user_cache


class CustomUserChangePass(forms.Form):
    email = forms.EmailField(
    widget=forms.EmailInput(attrs={'autofocus': 'True', 'placeholder': 'Email'}))
    

class CustomSetPass(forms.Form):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'}),
        strip=False,
    )
    new_password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}),
        strip=False,
    )
    
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)
    
    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2 and password1 != password2:
            self.add_error(None, {"new_password2": self.error_messages['password_mismatch'], "new_password1": ''})
        try:
            password_validation.validate_password(password2, self.user)
        except ValidationError as error:
            self.add_error(None, {"new_password2": error, "new_password1": ''})
        return password2
    
    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user
    

class TasksForm(forms.ModelForm):
    class Meta:
        model = Tasks
        fields = ('title', 'task', 'deadline')
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'Name your task'}),
            'task': TinyMCE(attrs={'placeholder': 'Describe your task', 'cols': 20, 'rows': 5}),
            'deadline': forms.DateInput(attrs={'type': 'date'}),
        }