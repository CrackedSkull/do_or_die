from django.urls import re_path, path
from . import views
from django.views.generic import RedirectView

urlpatterns = [
    re_path('^$', RedirectView.as_view(url='/welcome/')),
    re_path('^welcome/$', views.MainView.as_view(), name='main_page'),
    re_path('^reg/$', views.SignUpFormView.as_view(), name='reg'),
    re_path('^logout/$', views.LogtView.as_view(), name='logt'),
    re_path('^change_pass_email/$', views.ChangePasswordEmail.as_view(), name='change_pass_email'),
    path('<slug:action>/<slug:uidb64>/<slug:token>/', views.activate, name='approve'),
    re_path('^change_pass/$', views.ChangePassword.as_view(), name='change_pass'),
    re_path('^auth_change_pass/$', views.AuthChangePass.as_view(), name='auth_change_pass'),
    re_path('^your_tasks/$', views.ResultView.as_view(), name='content'),
    re_path('^archive/$', views.ResultView.as_view(), name='archive'),
    re_path('^add_new/$', views.AddNewTaskView.as_view(), name='new_task'),
    re_path('^change/$', views.ChangeView.as_view(), name='change'),
    re_path('^done_task/$', views.done_task, name='done'),
    re_path('^delete_task/$', views.delete_task, name='delete'),
]
