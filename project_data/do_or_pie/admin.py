from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm, TasksForm
from .models import CustomUser, Tasks


class CustomerUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('email', 'is_staff', 'is_active',)
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}))
    search_fields = ('email',)
    ordering = ('email',)
    
    
class CustomerTaskAdmin(admin.ModelAdmin):
    add_form = TasksForm
    form = TasksForm
    model = Tasks
    list_display = ('person', 'title', 'task', 'created_date', 'deadline', 'status')
    list_filter = ('person', 'status')
    search_fields = ('title', 'task', 'created_date', 'deadline', 'status')
    ordering = ('person', 'status')


admin.site.register(CustomUser, CustomerUserAdmin)
admin.site.register(Tasks, CustomerTaskAdmin)
