from django.shortcuts import render, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import FormView, View, TemplateView
from django.contrib.auth import login, logout
from .forms import CustomUserCreationForm, CustomAuthForm, TasksForm, CustomUserChangePass, CustomSetPass
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str
from .tokens import account_activation_token
from django.core.mail import send_mail
from .models import CustomUser, Tasks
from django.core.exceptions import PermissionDenied
from django.urls.exceptions import Resolver404
from django.http.response import Http404
from django.template.loader import get_template
import datetime


class MainView(FormView):
    form_class = CustomAuthForm
    template_name = 'welcome.html'
    success_url = reverse_lazy('main_page')

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('content'))
        else:
            return super(MainView, self).get(request)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(MainView, self).form_valid(form)


class SignUpFormView(FormView):
    form_class = CustomUserCreationForm
    template_name = 'sign_up.html'
    success_url = reverse_lazy('reg')

    def get(self, request, *args, **kwargs):
        message_action = request.GET.get('message', '')
        if message_action == 'Congratulation':
            return render(request=request,
                          template_name='message.html',
                          context={'message': '''Congratulations. Now you need to activate your account.
                          Follow the instructions in the letter that was sent to your email.'''})
        elif message_action == 'Error':
            return render(request=request,
                          template_name='message.html',
                          context={'message': '''We so sorry, but we have problem with activation via email. 
                          Please try later and you can write to our support team'''})
        else:
            return super(SignUpFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        current_site = get_current_site(self.request)
        mail_subject = 'Activation'
        context = {
            'action': 'Activate',
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        }
        message_html = get_template('email_link.html').render(context)
        message_plain = render_to_string('email_plain_text.html', context)
        to_email = form.cleaned_data.get('email')
        email = send_mail(
            mail_subject, message_plain, from_email='CrackedSkull@protonmail.com', recipient_list=[to_email], html_message=message_html,
            fail_silently=True
        )
        if email:
            return HttpResponseRedirect(self.get_success_url() + '?message=Congratulation')
        mail_subject = 'Error'
        message = render_to_string('error_msg.html', {'datetime': str(datetime.datetime.now()),
                                                      'exception': 'Can\'t send activation link'})
        _ = send_mail(
            mail_subject, message, from_email='CrackedSkull@protonmail.com', recipient_list=['CrackedSkull@protonmail.com',], fail_silently=True
        )
        return HttpResponseRedirect(self.get_success_url() + '?message=Error')


def activate(request, action, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, CustomUser.DoesNotExist):
        user = None
    if action == 'Activate':
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            return HttpResponseRedirect(reverse_lazy('main_page'))
        else:
            return render(request, 'message.html', {'message': 'Activation link invalid'})
    elif action == 'Password':
        if user is not None and account_activation_token.check_token(user, token):
            request.session['actual_user'] = user.email
            return HttpResponseRedirect(reverse_lazy('change_pass'))
        else:
            return render(request, 'message.html', {'message': 'Change link invalid'})


class ChangePasswordEmail(FormView):
    form_class = CustomUserChangePass
    template_name = 'password.html'
    success_url = reverse_lazy('change_pass_email')

    def get(self, request, *args, **kwargs):
        message_action = request.GET.get('message', '')
        if message_action == 'Change':
            return render(request=request,
                          template_name='message.html',
                          context={'message': '''Link for change password has been sent'''})
        elif message_action == 'Error':
            return render(request=request,
                          template_name='message.html',
                          context={'message': '''We so sorry, but we have problem with activation via email. 
                          Please try later and you can write to our support team'''})
        else:
            form = self.get_form()
            return self.render_to_response(context={'form': form})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        take_email = request.POST.get('email')
        try:
            user = CustomUser.objects.get(email=take_email)
        except CustomUser.DoesNotExist:
            form.add_error('email', 'First of all you must be our user')
            return super(ChangePasswordEmail, self).form_invalid(form)
        current_site = get_current_site(self.request)
        mail_subject = 'Change password'
        context = {
            'action': 'Password',
            'domain': current_site.domain,
            'token': account_activation_token.make_token(user),
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        }
        message_html = get_template('email_link.html').render(context)
        message_plain = render_to_string('email_plain_text.html', context)
        to_email = form.cleaned_data.get('email')
        email = send_mail(
            mail_subject, message_plain, from_email='CrackedSkull@protonmail.com', recipient_list=[to_email], html_message=message_html,
            fail_silently=True
        )
        if email:
            return HttpResponseRedirect(self.get_success_url() + '?message=Change')
        mail_subject = 'Error'
        message = render_to_string('error_msg.html', {'datetime': str(datetime.datetime.now()),
                                                      'exception': 'Can\'t send change link'})
        _ = send_mail(
            mail_subject, message, from_email='CrackedSkull@protonmail.com', recipient_list=['CrackedSkull@protonmail.com',], fail_silently=True
        )
        return HttpResponseRedirect(self.get_success_url() + '?message=Error')


class ChangePassword(FormView):
    form_class = CustomSetPass
    template_name = 'password.html'
    success_url = reverse_lazy('main_page')

    def get_form_kwargs(self):
        kwargs = super(ChangePassword, self).get_form_kwargs()
        kwargs['user'] = CustomUser.objects.get(email=self.request.session['actual_user'])
        return kwargs

    def get(self, request, *args, **kwargs):
        form = self.get_form()
        return render(request, 'password.html', {'form': form, 'approve': True})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return super(ChangePassword, self).form_invalid(form)

    def form_valid(self, form):
        form.save()
        del self.request.session['actual_user']
        return super(ChangePassword, self).form_valid(form)


class AuthChangePass(FormView):
    form_class = CustomSetPass
    template_name = 'password.html'
    success_url = reverse_lazy('main_page')

    def get_form_kwargs(self):
        kwargs = super(AuthChangePass, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            form = self.get_form()
            return render(request, 'password.html', {'form': form})
        else:
            raise PermissionDenied()

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return super(AuthChangePass, self).form_invalid(form)

    def form_valid(self, form):
        form.save()
        return super(AuthChangePass, self).form_valid(form)


class LogtView(View):
    @staticmethod
    def get(request):
        if request.user.is_authenticated:
            logout(request)
            return HttpResponseRedirect(reverse_lazy('main_page'))
        else:
            raise PermissionDenied()


class ResultView(TemplateView):
    template_name = 'actual_task.html'

    def get_context_data(self, **kwargs):
        if self.request.path == reverse_lazy('archive'):
            kwargs['data'] = self.request.user.person_task.filter(status=True)
            self.template_name = 'archive.html'
        elif self.request.path == reverse_lazy('content'):
            kwargs['data'] = self.request.user.person_task.filter(status=False)
            self.template_name = 'actual_task.html'
        kwargs['nickname'] = self.request.user.email[0:self.request.user.email.find('@')]
        return super(ResultView, self).get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return self.render_to_response(self.get_context_data())
        else:
            raise PermissionDenied()


class AddNewTaskView(FormView):
    form_class = TasksForm
    success_url = reverse_lazy('content')
    template_name = 'task_work.html'

    def get_context_data(self, **kwargs):
        kwargs['add'] = True
        return super(AddNewTaskView, self).get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return self.render_to_response(self.get_context_data())
        else:
            raise PermissionDenied()

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        else:
            raise PermissionDenied()

    def form_valid(self, form):
        task = form.save(commit=False)
        task.person = self.request.user
        task.save()
        return super(AddNewTaskView, self).form_valid(form)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class ChangeView(FormView):
    form_class = TasksForm
    template_name = 'task_work.html'
    success_url = reverse_lazy('content')

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            ctx = {}
            try:
                task = request.user.person_task.get(id=request.GET.get('task_id'), status=False)
            except Tasks.DoesNotExist:
                raise Http404('Before you can change task, you must create it')
            form = self.get_form()
            form['title'].initial = task.title
            form['task'].initial = task.task
            form['deadline'].initial = task.deadline
            ctx['form'] = form
            ctx['task_id'] = request.GET.get('task_id')
            return self.render_to_response(context=ctx)
        else:
            raise PermissionDenied()

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
            else:
                return super(ChangeView, self).form_invalid(form)
        else:
            raise PermissionDenied()

    def form_valid(self, form):
        try:
            task = self.request.user.person_task.get(id=self.request.POST.get('task_id'), status=False)
        except Tasks.DoesNotExist:
            raise Http404('Before you can change task, you must create it')
        task.title = form.cleaned_data['title']
        task.task = form.cleaned_data['task']
        task.deadline = form.cleaned_data['deadline']
        task.save()
        return super(ChangeView, self).form_valid(form)


def done_task(request):
    if request.user.is_authenticated:
        try:
            task = Tasks.objects.get(id=request.GET.get('task_id'), person=request.user)
        except Tasks.DoesNotExist:
            raise Http404('Before you can change task status, you must create it')
        task.status = True
        task.save()
        return HttpResponseRedirect(reverse_lazy('content'))
    else:
        raise PermissionDenied()


def delete_task(request):
    if request.user.is_authenticated:
        try:
            task = Tasks.objects.get(id=request.GET.get('task_id'), person=request.user)
        except Tasks.DoesNotExist:
            raise Http404('Before you can delete task, you must create it')
        status = task.status
        task.delete()
        if status:
            return HttpResponseRedirect(reverse_lazy('archive'))
        else:
            return HttpResponseRedirect(reverse_lazy('content'))
    else:
        raise PermissionDenied()


def custom_404(request, exception):
    if isinstance(exception, Resolver404):
        response = render(request, 'message.html', {'name': '404 Page Not Found',
                                                    'message': 'What you are looking for?',
                                                    'pic': '404.gif'})
    else:
        response = render(request, 'message.html', {'message': exception})
    response.status_code = 404
    return response


def custom_403(request, exception):
    response = render(request, 'message.html', {'name': '403 Forbidden',
                                                'message': 'Oh, you thought you were smart',
                                                'pic': 'motherfucker.gif'})
    response.status_code = 403
    return response


def custom_500(request):
    response = render(request, 'message.html', {'name': '500 Server Error',
                                                'message': 'Remember us. We will return later',
                                                'pic': 'return.gif'})
    response.status_code = 500

    mail_subject = 'Error'
    message = render_to_string('error_msg.html', {'datetime': str(datetime.datetime.now()),
                                                  'exception': str(request)})
    _ = send_mail(
        mail_subject, message, from_email='CrackedSkull@protonmail.com', recipient_list=['CrackedSkull@protonmail.com', ],
        fail_silently=True
    )
    return response
